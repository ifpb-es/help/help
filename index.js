/**
 * @format
 */
import React from 'react'
import { Provider } from 'react-redux'
import {AppRegistry} from 'react-native';
//import App from './App';
//import Navigator from './src/screens/CadastrarUsuario';
import Navigator from './src/screens/Navigator';
//import Navigator from './src/screens/App2';
import {name as appName} from './app.json';

import storeConfig from './src/store/storeConfig'

import axios from 'axios'

axios.defaults.baseURL = 'https://help-c5b9c.firebaseio.com/'

const store = storeConfig()

const Redux = () => (
    <Provider store={store}>
        <Navigator />
    </Provider>
)

AppRegistry.registerComponent(appName, () => Redux);
