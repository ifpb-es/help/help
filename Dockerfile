FROM centos:centos7
MAINTAINER Pedro Pacheco <pedrovcpacheco@yahoo.com>
WORKDIR /opt
RUN yum clean all &&  yum  install -y \
    epel-release  \
    git  \
    sudo \
    curl \
    net-tools \
    && curl -sL https://rpm.nodesource.com/setup_12.x | bash -  \
    && yum  install -y nodejs  \
    && rm -rf /var/cache/yum   \
    && git clone  https://gitlab.com/ifpb-es/help/help.git
WORKDIR /opt/help/socketio
RUN npm install 
ENTRYPOINT ["/usr/bin/node"]
CMD ["index.js"]


