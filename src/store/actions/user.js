import { USER_LOGGED_IN, 
         USER_LOGGED_OUT,
         LOADING_USER,
         USER_LOADED,
         TIPO_USER,
         LISTA_AVALIACOES_TECNICOS,
         ATUALIZAR_ESTADO } from './actionTypes'

import axios from 'axios'

const authBaseURL = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty'

const API_KEY = 'AIzaSyApaddIkH16IEOkWOmJsc1925ZzXh8LUaQ'

export const userLogged = user => {
    return {
        type: USER_LOGGED_IN,
        payload: user
    }
}

export const logout = () => {
    return {
        type: USER_LOGGED_OUT
    }
}

export const listaAvaliacoesTecnicos = listaAvaliacoes => {
    return {
        type: LISTA_AVALIACOES_TECNICOS,
        payload: listaAvaliacoes
    }
}

export const createUser = user => {

    return dispatch => {
        console.log('email', user.email)
        console.log('password', user.password)

        //axios.post(`${authBaseURL}/signupNewUser?key=${API_KEY}`, {
        axios.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyApaddIkH16IEOkWOmJsc1925ZzXh8LUaQ', {    
            email: user.email,
            password: user.password,
            returnSecureToken: true
        })
            .catch(err => console.log(err))
            .then(res => {

                if(res.data.localId){
                    axios.put(`/users/${res.data.localId}.json`,{
                        name:user.name,
                        cpf:user.cpf,
                        dataNascimento:user.dataNascimento,
                        cep:user.cep,
                        logradouro:user.logradouro,
                        numero:user.numero,
                        telefone:user.telefone,
                        meioPagamento:user.meioPagamento
                    })
                        .catch(err => console.log(err))
                        .then(res => {
                            console.log('Usuário criado com sucesso')
                        })
                }
            })
    }

}

export const createTecnico = user => {

    return dispatch => {
        console.log('email', user.email)
        console.log('password', user.password)

        //axios.post(`${authBaseURL}/signupNewUser?key=${API_KEY}`, {
        axios.post('https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyApaddIkH16IEOkWOmJsc1925ZzXh8LUaQ', {    
            email: user.email,
            password: user.password,
            returnSecureToken: true
        })
            .catch(err => console.log(err))
            .then(res => {

                if(res.data.localId){
                    axios.put(`/tecnicos/${res.data.localId}.json`,{
                        name:user.name,
                        cpfCnpj:user.cpfCnpj,
                        cep:user.cep,
                        logradouro:user.logradouro,
                        numero:user.numero,
                        telefone:user.telefone,
                        qualificacao:user.qualificacao,
                        email:user.email,
                        password:user.password,
                        valorServico:''
                    })
                        .catch(err => console.log(err))
                        .then(res => {
                            console.log('Técnico criado com sucesso')
                        })
                }
            })
    }

}

export const loadingUser = user => {
    return {
        type: LOADING_USER
    }
}

export const userLoaded = user => {
    return {
        type: USER_LOADED,
        payload: user
    }
}

export const carregarTipoUser = user => {
    return {
        type: TIPO_USER,
        payload: user
    }
}

export const login = user => {
    return dispatch => {
        dispatch(loadingUser())
        axios.post(`${authBaseURL}/verifyPassword?key=${API_KEY}`, {
              email: user.email,
              password: user.password,
              returnSecureToken: true
        })
          .catch(err => console.log(err))
          .then( res => {
              if(res.data.localId){
                  axios.get(`/users/${res.data.localId}.json`)
                      .catch(err => console.log(err))
                      .then(res => {
                          //user.password = null
                          user.password = res.data.password
                          user.name = res.data.name
                          dispatch(userLogged(user))
                          dispatch(userLoaded())
                      })
              }
          })
    }
    
}

export const loginTecnico = user => {
    return dispatch => {
        dispatch(loadingUser())
        axios.post(`${authBaseURL}/verifyPassword?key=${API_KEY}`, {
              email: user.email,
              password: user.password,
              returnSecureToken: true
        })
          .catch(err => console.log(err))
          .then( res => {
              if(res.data.localId){
                  axios.get(`/tecnicos/${res.data.localId}.json`)
                      .catch(err => console.log(err))
                      .then(res => {
                          //user.password = null
                          user.password = res.data.password
                          user.name = res.data.name
                          dispatch(userLogged(user))
                          dispatch(userLoaded())
                      })
              }
          })
    }
    
}

export const cadastrarServicoTecnico = user => {
    return dispatch => {
        //dispatch(loadingUser())
        console.log('user.email ', user.email)
        console.log('user.password ', user.password)
        console.log('user.valorServico ', user.valorServico)

        axios.post(`${authBaseURL}/verifyPassword?key=${API_KEY}`, {
              email: user.email,
              password: user.password,
              returnSecureToken: true
        })
          .catch(err => console.log('Erro1 ', err))
          .then( res => {
              if(res.data.localId){
                  axios.patch(`/tecnicos/${res.data.localId}.json`,{
                    valorServico:user.valorServico,
                    
                })
                    .catch(err => console.log('Erro2 ',err))
                    .then(res => {
                        console.log('Servico do tecnico cadastrado com sucesso!')
                    })
            }
        })   
          
    }
    
}

 export const createAvaliacaoTecnico  = avaliacaoTecnico => {
    return dispatch => {
        //dispatch(loadingUser())
        console.log('user.email ', avaliacaoTecnico.emailCliente)
        console.log('user.password ', avaliacaoTecnico.passwordCliente)
        console.log('user.notaAvaliacao ', avaliacaoTecnico.notaAvaliacaoTecnico)
        console.log('user.nomeTecnico ', avaliacaoTecnico.nameTecnico)

        axios.post(`${authBaseURL}/verifyPassword?key=${API_KEY}`, {
              email: avaliacaoTecnico.emailCliente,
              password: avaliacaoTecnico.passwordCliente,
              returnSecureToken: true
        })
          .catch(err => console.log('Erro1 ', err))
          .then( res => {
              if(res.data.localId){
                  axios.post(`/avaliacoesTecnicos/${res.data.localId}.json`,{
                    notaAvaliacao:avaliacaoTecnico.notaAvaliacaoTecnico,
                    nomeTecnico:avaliacaoTecnico.nameTecnico
                })
                    .catch(err => console.log('Erro2 ',err))
                    .then(res => {
                        console.log('Nota do tecnico cadastrado com sucesso!')
                    })
            }
        })   
          
    }
    
} 


export const obterAvaliacoesTecnico = user => {
    return dispatch => {
        //dispatch(loadingUser())
        axios.post(`${authBaseURL}/verifyPassword?key=${API_KEY}`, {
              email: user.email,
              password: user.password,
              returnSecureToken: true
        })
          .catch(err => console.log(err))
          .then( res => {
              if(res.data.localId){
                  //axios.get(`/avaliacoesTecnicos/${res.data.localId}.json`)
                  axios.get(`/avaliacoesTecnicos.json`)
                      .catch(err => console.log(err))
                      .then(res => {
                          //user.password = null
                          //user.password = res.data.password
                          //user.name = res.data.name
                          console.log('res', res.data)
                          dispatch(listaAvaliacoesTecnicos(res.data))
                          
                          //dispatch(userLoaded())
                      })
              }
          })
    }
    
}


export const entrarTipoUsuario = user => {
    return dispatch => {
        console.log("Tipo User 2 ",user.tipoUser)
        dispatch(carregarTipoUser(user))        
    }    
}