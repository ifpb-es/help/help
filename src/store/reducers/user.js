import { USER_LOGGED_IN, 
         USER_LOGGED_OUT,
         LOADING_USER,
         USER_LOADED,
         TIPO_USER,
         LISTA_AVALIACOES_TECNICOS } from '../actions/actionTypes'

const initialState = {
    name: null,
    email: null,
    isLoading: false,
    tipoUser: null,
    listaAvaliacoes: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGGED_IN:
            return{
                ...state,
                name: action.payload.name,
                email: action.payload.email,
                password: action.payload.password
            }
        case USER_LOGGED_OUT:
            return{
                ...state,
                name: null,
                email: null
            }
        case LOADING_USER:
            return{
                ...state,
                isLoading: true
            } 
        case USER_LOADED:
            return{
                ...state,
                isLoading: false
            }  
        case TIPO_USER:
        return{
            ...state,
            tipoUser: action.payload.tipoUser
        } 
        case LISTA_AVALIACOES_TECNICOS:
        return{
            ...state,
            listaAvaliacoes: action.payload
        }
        default: 
           return state
    }
}

export default reducer