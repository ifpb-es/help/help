import React, { Component } from 'react';
import { StyleSheet, 
         Text, 
         View, 
         Button, 
         Alert,
         TouchableOpacity,
         Modal, 
         TouchableHighlight} from 'react-native';

import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import Pusher from 'pusher-js/react-native';
import RNGooglePlacePicker from 'react-native-google-place-picker';
import Geocoder from 'react-native-geocoding';
import MapView from 'react-native-maps';
import Spinner from 'react-native-loading-spinner-overlay';
import Geolocation from '@react-native-community/geolocation';
import messaging from '@react-native-firebase/messaging'
import io from 'socket.io-client'
import { Rating, AirbnbRating } from 'react-native-ratings';

	
import AsyncStorage from '@react-native-community/async-storage';
import { regionFrom, getLatLonDiffInMeters } from './helpers';

//import * as admin from "firebase-admin";

//importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js');
//importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-messaging.js');

var serviceAccount = require("../config/help-c5b9c-firebase-adminsdk-k7rw9-246c8edb87.json");

import { createAvaliacaoTecnico } from '../store/actions/user'




//Geocoder.setApiKey('YOUR GOOGLE SERVER API KEY');
import axios from 'axios'
axios.defaults.baseURL = 'https://help-c5b9c.firebaseio.com/'

Geocoder.init("AIzaSyBUhAl920ZBdRyVBDXw3JgYclaPyf4EMvs");

class App2 extends Component {

  state = {
    location: null,
    error: null,
    has_ride: false,
    destination: null,
    driver: null,
    origin: null,
    is_searching: false,
    has_ridden: false,
    initialPosition: 'unknown',
    lastPosition: 'unknown',
    regiaoTecnico: null,
    isStarAberto: false,
    ratingState: 2,
    isTecnicoConcluiuServico: false,
    modalVisible: false,
    nameTecnico: ''
  };

	constructor() {
  	super();
    this.username = 'wernancheta';
  	this.available_drivers_channel = null;
    this.user_ride_channel = null;
    this.teste3 = this.teste3.bind(this)
    this.localizacaoTecnicoAtual = this.localizacaoTecnicoAtual.bind(this)
    this.ratingCompleted = this.ratingCompleted.bind(this)
    //this.enviarAvaliacao = this.enviarAvaliacao(this)
	}

  

  teste3(){
    
    this.setState({
      is_searching: true,
      driver: false
    }); 

    setInterval(() => {
      this.setState({
        is_searching: false,
        driver: true 
      });
     }, 4000);

    
      
  }
  
  ratingCompleted(rating) {
    console.log("Rating is: " + rating)
    this.setState({
      //isStarAberto: false,
      ratingState: rating
    });
  }

  enviarAvaliacao(){
    //Alert.alert('state.ratingState', this.state.ratingState)
    console.log('state.ratingState', this.state.ratingState)
    this.setState({
      isStarAberto: false,
      driver: false
    });

    let email1 = this.props.email
    let password1 = this.props.password

    console.log('email1', email1)
    console.log('password1', password1)

    let avaliacaoTecnico = {
        nameTecnico: this.state.nameTecnico,
        notaAvaliacaoTecnico: this.state.ratingState,
        emailCliente: email1,
        passwordCliente: password1
    }

    console.log('emailCliente', avaliacaoTecnico.emailCliente)
    console.log('notaAvaliacaoTecnico', avaliacaoTecnico.notaAvaliacaoTecnico)

    this.props.onCreateAvaliacaoTecnico(avaliacaoTecnico)

  }


  _setCurrentLocation() {

  	//navigator.geolocation.getCurrentPosition(
    Geolocation.getCurrentPosition(
      (position) => {
        var region = regionFrom(
          position.coords.latitude, 
          position.coords.longitude, 
          position.coords.accuracy
        );
        
          console.log('lat', position.coords.latitude)
          console.log('long', position.coords.longitude)

        //Geocoder.getFromLatLng(position.coords.latitude, position.coords.longitude).then(
        Geocoder.from(position.coords.latitude, position.coords.longitude).then(
          (json) => {
            var address_component = json.results[0].address_components[0];
            
            this.setState({
              origin: {
                name: address_component.long_name,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
                //latitude:-7.088141, 
                //longitude:-34.841119
              },
              location: region,
              destination: null,
              has_ride: false,
              has_ridden: false,
              driver: null    
            });

          },
          (error) => {
            console.log('err geocoding: ', error);
          }
        );

      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 3000 },
  	);

  }

  componentWillUnmount(){
    this.setState({
      is_searching: false
    })
  }

  componentDidMount() {


    this._setCurrentLocation();

    //this.socket = io("http://192.168.0.102:3000")
    this.socket = io("http://helpes.duckdns.org/")

    //colocar esse listener no motorista
    this.socket.on("localizacaoTecnico", msg => {
      
       console.log('msg cliente!!', msg)
       console.log('msg', JSON.stringify(msg))

      let msgString = JSON.stringify(msg)

      if(msgString.indexOf('latitudeTecnico') > -1){
        
        msgString = msgString.replace('latitudeTecnico', 'latitude')
        msgString = msgString.replace('longitudeTecnico', 'longitude')

        msg = JSON.parse(msgString)

        console.log('msg json', msg)


        this.setState({
          regiaoTecnico: msg,
          driver: true,
          is_searching: false,
          modalVisible: true
        })

        console.log('state.regiaoTecnico', this.state.regiaoTecnio)
      } else if(JSON.stringify(msg).indexOf('isServicoFinalizado') > -1){   
        if(JSON.stringify(msg).indexOf('true') > -1){ 
              this.setState({isStarAberto: true,
                              nameTecnico: msg.nameTecnico})              
        }
      }


    })

  }

  localizacaoTecnicoAtual(){
    let localizacao = {
      latitudeCliente: -7.088753, 
      longitudeCliente: -34.840560}
    //console.log('this.socket', this.socket)

    this.socket.emit("localizacaoTecnico", localizacao)

    //this.setState({is_searching: true})
    this.setState({is_searching: true})
  }

  render() {


    return (
      <View style={styles.container}>

      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>A avaliação média técnico é : </Text>

            <AirbnbRating
          count={5}
          reviews={[ "Ruim", "Ok", "Bom", "Ótimo", "Fantástico"]}
          defaultRating={4}
          size={20}
          onFinishRating={this.ratingCompleted}
          isDisabled={true}
        /> 

            <TouchableHighlight
              style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
              onPress={() => {
                this.setState({modalVisible: false})
              }}
            >
              <Text style={styles.textStyle}>Ok</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>


      	<Spinner 
      		visible={this.state.is_searching} 
      		textContent={"Procurando profissional..."} 
      		textStyle={{color: '#FFF'}} />
        <View style={styles.header}>
        <View style={styles.iconBar}>
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='bars' 
                    size={20} color='#000'/>
            </TouchableOpacity>
        </View>
          <Text style={styles.header_text}>Help</Text>
        </View>

        { this.state.isStarAberto &&
        <View style={styles.form_container2}>
        
        <AirbnbRating
          count={5}
          reviews={[ "Ruim", "Ok", "Bom", "Ótimo", "Fantástico"]}
          defaultRating={2}
          size={20}
          onFinishRating={this.ratingCompleted}
        /> 
          <Button
              onPress={this.enviarAvaliacao.bind(this)}
              title="Enviar Avaliação"
              color="#103D50"             
            />
        
        </View>
        }
        {
          !this.state.has_ride && 
          <View style={styles.form_container}>
            <Button
              onPress={this.localizacaoTecnicoAtual}
              title="Buscar Profissional"
              color="#103D50"
              //style={{backgroundColor: '#f4b042', borderRadius:10}}              
            />
          </View>
        }
        
        <View style={styles.map_container}>  
        {
          this.state.origin && this.state.destination &&
          <View style={styles.origin_destination}>
            <Text style={styles.label}>Origin: </Text>
            <Text style={styles.text}>{this.state.origin.name}</Text>
           
            <Text style={styles.label}>Destination: </Text>
            <Text style={styles.text}>{this.state.destination.name}</Text>
          </View>  
        }
        {
          //this.state.location &&
          <MapView
            provider={MapView.PROVIDER_GOOGLE}
            style={styles.map}
            //region={this.state.location}
            region={{
              latitude: -7.088753,
              longitude: -34.840560,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            {
              //this.state.origin && !this.state.has_ridden &&
              <MapView.Marker
                //coordinate={{
                //latitude: this.state.origin.latitude, 
                //longitude: this.state.origin.longitude}}
                coordinate={{
                  latitude: -7.088753, 
                  longitude: -34.840560}}
                title={"You're here"}
              />
            }
    
            {
              this.state.driver &&
              <MapView.Marker
                //coordinate={{
                //latitude: this.state.driver.latitude, 
                //longitude: this.state.driver.longitude}}
                coordinate={this.state.regiaoTecnico}
                title={"Your driver is here"}
                pinColor={"#4CDB00"}
              />
            }
          </MapView>
        }
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end'
  },
  form_container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    paddingBottom:30
  },
  form_container2: {
    alignItems: 'center',
    padding: 10
  },
  header: {
    padding: 20,
    //backgroundColor: '#333',
    backgroundColor: '#f4b042',       
  },
  header_text: {
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold'
  },  
  origin_destination: {
    alignItems: 'center',
    padding: 10
  },
  label: {
    fontSize: 18
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  map_container: {
    flex: 9
  },
  map: {
   flex: 1
  },
  iconBar : {
      flexDirection : 'row',
      marginHorizontal: 0,
      justifyContent: 'space-between',
      marginTop: 10
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    marginTop:15
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

const mapStateToProps = ({user}) => {
  return {
      tipoUser: user.tipoUser,
      email: user.email,
      password: user.password        
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCreateAvaliacaoTecnico: avaliacaoTecnico => dispatch(createAvaliacaoTecnico(avaliacaoTecnico))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App2)