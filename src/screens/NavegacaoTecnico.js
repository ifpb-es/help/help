import React, { Component } from 'react';
import { StyleSheet, 
         Text, 
         View, 
         Button, 
         Alert,
         TouchableOpacity} from 'react-native';

import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome'
import Pusher from 'pusher-js/react-native';
import RNGooglePlacePicker from 'react-native-google-place-picker';
import Geocoder from 'react-native-geocoding';
import MapView from 'react-native-maps';
import Spinner from 'react-native-loading-spinner-overlay';
import Geolocation from '@react-native-community/geolocation';
import messaging from '@react-native-firebase/messaging'
import io from 'socket.io-client'

	
import AsyncStorage from '@react-native-community/async-storage';
import { regionFrom, getLatLonDiffInMeters } from './helpers';

//import * as admin from "firebase-admin";

//importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js');
//importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-messaging.js');

var serviceAccount = require("../config/help-c5b9c-firebase-adminsdk-k7rw9-246c8edb87.json");



//Geocoder.setApiKey('YOUR GOOGLE SERVER API KEY');
import axios from 'axios'
axios.defaults.baseURL = 'https://help-c5b9c.firebaseio.com/'

Geocoder.init("AIzaSyBUhAl920ZBdRyVBDXw3JgYclaPyf4EMvs");

class NavegacaoTecnico extends Component {

  state = {
    location: null,
    error: null,
    has_ride: false,
    destination: null,
    driver: null,
    origin: null,
    is_searching: false,
    has_ridden: false,
    initialPosition: 'unknown',
    lastPosition: 'unknown',
    regiaoCliente: null,
    tem_cliente: false
  };

	constructor() {
  	super();
    this.username = 'wernancheta';
  	this.available_drivers_channel = null;
    this.user_ride_channel = null;
    this.teste3 = this.teste3.bind(this)
    this.aceitarSolicitacao = this.aceitarSolicitacao.bind(this)
    this.finalizarServico = this.finalizarServico.bind(this) 
	}

  

  teste3(){
    
    this.setState({
      is_searching: true,
      driver: false
    }); 

    setInterval(() => {
      this.setState({
        is_searching: false,
        driver: true 
      });
     }, 4000);

    
      
  }


  _setCurrentLocation() {

  	//navigator.geolocation.getCurrentPosition(
    Geolocation.getCurrentPosition(
      (position) => {
        var region = regionFrom(
          position.coords.latitude, 
          position.coords.longitude, 
          position.coords.accuracy
        );
        
          console.log('lat', position.coords.latitude)
          console.log('long', position.coords.longitude)

        //Geocoder.getFromLatLng(position.coords.latitude, position.coords.longitude).then(
        Geocoder.from(position.coords.latitude, position.coords.longitude).then(
          (json) => {
            var address_component = json.results[0].address_components[0];
            
            this.setState({
              origin: {
                name: address_component.long_name,
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
                //latitude:-7.088141, 
                //longitude:-34.841119
              },
              location: region,
              destination: null,
              has_ride: false,
              has_ridden: false,
              driver: null    
            });

          },
          (error) => {
            console.log('err geocoding: ', error);
          }
        );

      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 10000, maximumAge: 3000 },
  	);

  }


  componentDidMount() {


    this._setCurrentLocation();

    //this.socket = io("http://192.168.0.102:3000")

    this.socket = io("http://helpes.duckdns.org/")

    //colocar esse listener no motorista
    this.socket.on("localizacaoTecnico", msg => {
    
        console.log('msgg tecnico!!!', msg)

    let msgString = JSON.stringify(msg)

    if(msgString.indexOf('latitudeCliente') > -1){

      let temCliente = this.state.tem_cliente
      
      //só aparece a tela de confirmação, se não já tiver confirmado antes
      if(!temCliente){     
       
          msgString = msgString.replace('latitudeCliente','latitude')
          msgString = msgString.replace('longitudeCliente','longitude')

          msg = JSON.parse(msgString)
          
          Alert.alert(
            //title
            'Confirmação',
            //body
            'Você tem um cliente. Você quer aceitar o serviço?',
            [
              {text: 'Sim', onPress: () => this.aceitarSolicitacao(msg)},
              {text: 'Não', onPress: () => console.log('No Pressed'), style: 'cancel'},
            ],
            { cancelable: false }
            //clicking out side of alert will not cancel
          )
    
      }
      
    }

    })

    //this.socket.emit("localizacaoTecnico", '')

  }

  aceitarSolicitacao(msg){
    
    // se solicitacao ja foi aceita por outro técnico
    if(JSON.stringify(msg).indexOf('isSolicitacaoAceita') > -1){    
      if(JSON.stringify(msg).indexOf('true') > -1){

      Alert.alert(
        //title
        '',
        //body
        'Solicitação já aceita por outro técnico! Aguarde o próximo cliente.',
        [
          {text: 'Ok', onPress: console.log('No Pressed'), style: 'cancel'}
        ],
        { cancelable: false }
        //clicking out side of alert will not cancel
      )}

    } else {

        this.setState({tem_cliente: true, 
                      regiaoCliente: msg})

        let localizacaoTecnico = {
          latitudeTecnico: -7.098305, 
          longitudeTecnico: -34.844953}

          //console.log('this.socket', this.socket)
          // coloca a situacao como aceita para os outros tecnicos não aceitarem
          let solicitacaoAceita = {
            isSolicitacaoAceita: true
          }

        this.socket.emit("localizacaoTecnico", localizacaoTecnico)
        this.socket.emit("localizacaoTecnico", solicitacaoAceita)
    }
  }

  finalizarServico() {
    this.setState({tem_cliente: false})

    let servicoFinalizado = {
      isServicoFinalizado: true,
      nameTecnico: this.props.name }

      //console.log('this.socket', this.socket)

    this.socket.emit("localizacaoTecnico", servicoFinalizado)
  }

  render() {

    return (
      <View style={styles.container}>
      	<Spinner 
      		visible={this.state.is_searching} 
      		textContent={"Procurando profissional..."} 
      		textStyle={{color: '#FFF'}} />
        <View style={styles.header}>
        <View style={styles.iconBar}>
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='bars' 
                    size={20} color='#000'/>
            </TouchableOpacity>
        </View>
          <Text style={styles.header_text}>Help</Text>
        </View>
         {
            this.state.tem_cliente && 
          <View style={styles.form_container}>
            <Button
              onPress={this.finalizarServico}
              title="Finalizar Serviço"
              color="#103D50"
            />
          </View>
        }
        
        <View style={styles.map_container}>  
        {
          this.state.origin && this.state.destination &&
          <View style={styles.origin_destination}>
            <Text style={styles.label}>Origin: </Text>
            <Text style={styles.text}>{this.state.origin.name}</Text>
           
            <Text style={styles.label}>Destination: </Text>
            <Text style={styles.text}>{this.state.destination.name}</Text>
          </View>  
        }
        {
         // this.state.location &&
          <MapView
            provider={MapView.PROVIDER_GOOGLE}
            style={styles.map}
            //region={this.state.location}
            region={{
              latitude: -7.098305,
              longitude:  -34.844953,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
          >
            {
              //this.state.origin && !this.state.has_ridden &&
              <MapView.Marker
                //coordinate={{
                //latitude: this.state.origin.latitude, 
                //longitude: this.state.origin.longitude}}
                coordinate={{
                  latitude: -7.098305, 
                  longitude: -34.844953}}
                title={"Você está aqui"}
              />
            }
    
            {
              this.state.tem_cliente &&
              <MapView.Marker
                //coordinate={{
                //latitude: this.state.driver.latitude, 
                //longitude: this.state.driver.longitude}}
                coordinate={this.state.regiaoCliente}
                title={"Seu cliente está aqui"}
                pinColor={"#4CDB00"}
              />
            }
          </MapView>
        }
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end'
  },
  form_container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  header: {
    padding: 20,
    //backgroundColor: '#333',
    backgroundColor: '#f4b042',
  },
  header_text: {
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold'
  },  
  origin_destination: {
    alignItems: 'center',
    padding: 10
  },
  label: {
    fontSize: 18
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  map_container: {
    flex: 9
  },
  map: {
   flex: 1
  },
  iconBar : {
      flexDirection : 'row',
      marginHorizontal: 0,
      justifyContent: 'space-between',
      marginTop: 10
  },
  map_view: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  }
});

const mapStateToProps = ({user}) => {
  return {
      tipoUser: user.tipoUser,
      email: user.email,
      password: user.password,
      name: user.name        
  }
}

export default connect(mapStateToProps, null)(NavegacaoTecnico)