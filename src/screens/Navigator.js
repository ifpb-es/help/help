import React from 'react'
import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createDrawerNavigator} from 'react-navigation-drawer'

import Login from './Login'
import CadastrarUsuario from './CadastrarUsuario'
import CadastrarTecnico from './CadastrarTecnico'
import Profile from './Profile'
import Servico from './Servico'
import HistoricoAvaliacaoTecnico from './HistoricoAvaliacaoTecnico'
import App2 from './App2'
import NavegacaoTecnico from './NavegacaoTecnico'
import OpcaoProfile from './OpcaoProfile'

//const loginOrProfileRouter = createSwitchNavigator( {
//    Profile: Profile,
 //   Auth:Login
//}, {
//    initialRouteName: 'Auth'
//})

const menuRoutes = {
    /* Login: {
        name: 'Login',
        screen: props => <Login {...props}/>,
        navigationOptions: {
            title: 'Login',

        }
    }, */
    OpcaoProfile: {
        name: 'OpcaoProfile',
        screen: OpcaoProfile,
        navigationOptions: {
            title: '',

        }
    },
    Profile: {
        name: 'Profile',
        screen: props => <Profile {...props}/>,
        navigationOptions: {
            title: 'Perfil',

        }
    },
    Servico: {
        name: 'Servico',
        screen: props => <Servico {...props}/>,
        navigationOptions: {
            title: 'Serviço',

        }
    },
    HistoricoAvaliacaoTecnico: {
        name: 'HistoricoAvaliacaoTecnico',
        screen: props => <HistoricoAvaliacaoTecnico {...props}/>,
        navigationOptions: {
            title: 'Histórico de Avaliações do Técnico',

        }
    },
    App2: {
        name: 'App2',
        screen: props => <App2 {...props}/>,
        navigationOptions: {
            title: '',
        }
    },
    NavegacaoTecnico: {
        name: 'NavegacaoTecnico',
        screen: props => <NavegacaoTecnico {...props}/>,
        navigationOptions: {
            title: '',
        }
    }
    /* CadastrarUsuario: {
        name: 'CadastrarUsuario',
        screen: props => <CadastrarUsuario {...props}/>,
        navigationOptions: {
            title: 'Cadastrar Usuario'
        }
    }  */ 

}

const menuNavigator = createDrawerNavigator(menuRoutes)


const mainRoutes = {
    Home: {
        name: 'Home',
        screen: menuNavigator
    },
    Login: {
        name: 'Login',
        screen: Login
    },
    CadastrarUsuario: {
        name: 'CadastrarUsuario',
        screen: CadastrarUsuario
    },
    OpcaoProfile: {
        name: 'OpcaoProfile',
        screen: OpcaoProfile
    },
    CadastrarTecnico: {
        name: 'CadastrarTecnico',
        screen: CadastrarTecnico
    }

}


const mainNavigator = createSwitchNavigator(mainRoutes, {
    initialRoutName: 'OpcaoProfile'
})

export default createAppContainer(mainNavigator)