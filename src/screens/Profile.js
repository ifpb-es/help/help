import React, { Component } from 'react'
import { View,
         Text,
         StyleSheet,
         TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import  { logout } from '../store/actions/user'

class Profile extends Component {
    
    componentDidMount() {
        console.log('nome',this.props.name)
        console.log('email',this.props.email)

    }
    
    logout = () => {
            this.props.onLogout()
            this.props.navigation.navigate('OpcaoProfile')
    }

    render () {
        //const options = {email: this.props.email, secure: true}
        return (
            <View style={StyleSheet.container}>
                <Text style={styles.nickname}>{this.props.name}</Text>
                <Text style={styles.email}>{this.props.email}</Text>
                <TouchableOpacity onPress={this.logout} style={styles.buttom}>
                    <Text style={styles.buttomText}>Sair</Text>  
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    nickname:{
        marginTop: 30,
        fontSize: 30,
        fontWeight: 'bold'
    },
    email: {
        marginTop: 20,
        fontSize: 25
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10
    },
    buttomText:{
        fontSize: 20, 
        color: '#FFF'
    }
})

const mapStateToProps = ({ user }) => {
    return {
        email: user.email,
        name: user.name
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(logout())
    }
}

//export default Profile

export default connect(mapStateToProps, mapDispatchToProps)(Profile)