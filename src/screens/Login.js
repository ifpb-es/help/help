import React, { Component } from 'react'
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ImageBackground
} from 'react-native'
import { connect } from 'react-redux'

import { login } from '../store/actions/user'
import { loginTecnico } from '../store/actions/user'

//import backCadastro from '../images/login3.JPG'
//import backCadastro from '../images/IMG_5984.jpg'

class Login extends Component {
    state = {
        name: 'Nome Temporario',
        email: '',
        password: ''
    }

    componentDidUpdate = prevProps => {
        if(prevProps.isLoading && !this.props.isLoading){
            //this.props.navigation.navigate('Profile')
            let tipoUsuario = this.props.tipoUser
            if(tipoUsuario == 'C') {
                this.props.navigation.navigate('App2')
            } else if (tipoUsuario == 'T') {
                this.props.navigation.navigate('NavegacaoTecnico')
            }
        }
    }

    login = () => {
       // this.props.onLogin({ ...this.state })
        //this.props.navigation.navigate('Profile')

        let tipoUsuario = this.props.tipoUser

        if(tipoUsuario == 'C') {
            this.props.onLogin({ ...this.state })
        } else if (tipoUsuario == 'T') {
             this.props.onLoginTecnico({ ...this.state })
        } 
    }

    cadastrarUsuario = () => {
        let tipoUsuario = this.props.tipoUser

        if(tipoUsuario == 'C') {
            this.props.navigation.navigate('CadastrarUsuario')
        } else if (tipoUsuario == 'T') {
            this.props.navigation.navigate('CadastrarTecnico')
        }
    }

    render () {
        return (
//<ImageBackground source={backCadastro} 
 //               style={styles.backGround}>
            <View style={styles.container}>

             

                <TextInput placeholder="Email" style={styles.input}
                    autoFocus={true} keyboardType='email-address'
                    value={this.state.email}
                    onChangeText={email => this.setState({email})} />
                <TextInput placeholder="Senha" style={styles.input}
                    secureTextEntry={true} value={this.state.password}
                    onChangeText={password => this.setState({password})} />   

                <TouchableOpacity onPress={this.login} style={styles.buttom}>
                    <Text style={styles.buttomText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.cadastrarUsuario} style={styles.buttom}>
                    <Text style={styles.buttomText}>Criar nova conta...</Text>
                </TouchableOpacity>
                
            
            </View>
           // </ImageBackground>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10        
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF',
    },
    input: {
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333', 
    },
    backGround: {
        flex: 1
    }
})

const mapStateToProps = ({user}) => {
    return {
        isLoading: user.isLoading,
        tipoUser: user.tipoUser        
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: user => dispatch(login(user)),
        onLoginTecnico: user => dispatch(loginTecnico(user))
    }
}

//export default Login
export default connect(mapStateToProps, mapDispatchToProps)(Login)