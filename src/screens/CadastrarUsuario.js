import React, {Component} from 'react'
import {     
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    DatePickerAndroid,
    Image,
    Alert,
    ImageBackground,
    ScrollView,
    Picker,
    CheckBox,
    Modal
} from 'react-native'
import { connect } from 'react-redux'
import { createUser } from '../store/actions/user'

import Icon from 'react-native-vector-icons/FontAwesome'
import RNPickerSelect from 'react-native-picker-select';

import moment from 'moment'

//const { cpfValidador } = require('cpf-cnpj-validator')
import { cpf } from 'cpf-cnpj-validator'

import backCadastro from '../images/IMG_5984.jpg'


class CadastrarUsuario extends Component{


    state ={
        name: '',
        cpf: '',
        dataNascimento: '',
        cep: '',
        logradouro: '',
        numero: '',
        telefone: '',
        meioPagamento: '',
        email: '',
        password: '',
        checked: false,
        abrirModal: false
    }

    handleDateAndroidChanged = () => {
        DatePickerAndroid.open({
            dataNascimento: this.state.dataNascimento
        }).then(e => {
           if( e.action !== DatePickerAndroid.dismissedAction) {
               const momentDate = moment(this.state.date)
               momentDate.date(e.day)
               momentDate.month(e.month)
               momentDate.year(e.year)

               this.setState({dataNascimento: momentDate.toDate()})

               this.props.dataNascimento = this.state.dataNascimento

               //console.warn(this.props.dataNascimento)
           }
        })
    }

    render(){

        console.log("Tipo USer:", this.props.tipoUser)

        return (

            

            <View style={styles.container}>

                <Modal visible={ this.state.abrirModal } animationType = "slide" 
                        onRequestClose={ () => console.log('closed') }/>

                <ImageBackground source={backCadastro} 
                    style={styles.backGround}>

                    {/* <View style={styles.iconBar}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name='bars' 
                                size={20} color='#000'/>
                        </TouchableOpacity>
                    </View> */}

                </ImageBackground>
                
                <View style={styles.formulario}>
                <ScrollView >
                    <TextInput placeholder='Nome' style={styles.input}
                    autoFocus={true} value={this.state.name}
                    onChangeText={name => this.setState({name})}/>
                    
                    <TextInput placeholder='Cpf*' style={styles.input}
                    autoFocus={false} value={this.state.cpf}
                    onChangeText={cpf => this.setState({cpf})}
                    keyboardType={'numeric'}/>

                    <View style={{flexDirection:"row"}}>
                    <TextInput  style={styles.input2}
                    autoFocus={false} value={this.state.dataNascimento ?
                        moment(this.state.dataNascimento).format('D/MM/YYYY') : ''}
                    onChangeText={dataNascimento => this.setState({dataNascimento})}
                    placeholder='Data de Nascimento*'
                    editable={false}
                    />
                    <TouchableOpacity onPress={this.handleDateAndroidChanged}>
                    <Icon name='calendar' style={{ paddingLeft: 7, paddingTop: 22 }}
                                size={30} color='#000'/>
                    
                    </TouchableOpacity>
                    </View>

                    <TextInput placeholder='Cep*' style={styles.input}
                    autoFocus={false} value={this.state.cep}
                    onChangeText={cep => this.setState({cep})}
                    keyboardType={'numeric'}/>

                    <TextInput placeholder='Logradouro*' style={styles.input}
                    autoFocus={false} value={this.state.logradouro}
                    onChangeText={logradouro => this.setState({logradouro})}/>

                    <TextInput placeholder='Numero*' style={styles.input}
                    autoFocus={false} value={this.state.numero}
                    onChangeText={numero => this.setState({numero})}
                    keyboardType={'numeric'}/>

                    <TextInput placeholder='Telefone' style={styles.input}
                    autoFocus={false} value={this.state.telefone}
                    onChangeText={telefone => this.setState({telefone})}
                    keyboardType={'numeric'}/>

                    <Picker selectedValue = {this.state.meioPagamento} 
                        onValueChange ={ meioPagamento => this.setState({ meioPagamento }) }
                        style={styles.select}>
                        <Picker.Item label="Meio de Pagamento*" value="MP" />  
                        <Picker.Item label="Dinheiro" value="D" />
                        <Picker.Item label="Cartão de Crédito" value="C" />
                    </Picker>

                    <TextInput placeholder='Email*' style={styles.input}
                    autoFocus={false} value={this.state.email}
                    onChangeText={email => this.setState({email})}/>

                    <TextInput placeholder='Senha*' style={styles.input}
                    autoFocus={false} value={this.state.password}
                    secureTextEntry={true}
                    onChangeText={password => this.setState({password})}/>

                    {/* <CheckBox
                            value={this.state.checked}
                            onValueChange={() => this.setState({checked: !this.state.checked})}
                            style={{alignSelf: "left"}}
                            />
                    <Text style={{margin: 8}} onPress={
                        () => this.setState({abrirModal: true})}>
                        Aceita o termo e condições de uso?</Text> */}

                    <TouchableOpacity onPress={this.salvar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cadastrar </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.cancelar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cancelar </Text>
                    </TouchableOpacity>

                </ScrollView>
            </View> 
            </View>    
        )
    }
    
    validarDados = () => {
          
        let nome = this.state.name
        let dataNascimento = this.state.dataNascimento
        let cep = this.state.cep
        let cpf2 = this.state.cpf
        let telefone = this.state.telefone
        let logradouro = this.state.logradouro
        let numero = this.state.numero
        let meioPagamento = this.state.meioPagamento
        let email = this.state.email
        let password = this.state.password

        let msgError = ''
        let entrouAlgumErro = false

        /* if( (!nome || nome.trim() == '')){

            msgError = 'Nome é obrigatório!'+ '\n'
            entrouAlgumErro = true

        } */
        
        if( (!cpf2 || cpf2.trim() == '')){

            msgError += 'Cpf é obrigatório!'+ '\n'
            entrouAlgumErro = true

            

        } else if(cpf2.length < 11 || cpf2.length > 11){
            msgError += 'CPF deve conter 11 dígitos!'+ '\n'
            entrouAlgumErro = true
        
        } else if(!cpf.isValid(cpf2)){
            
            msgError += 'CPF não é válido!'+ '\n'
            entrouAlgumErro = true
        }

        if( !dataNascimento){

            msgError += 'Data de Nascimento é obrigatório!'+ '\n'
            entrouAlgumErro = true

        } 
        
        if( (!cep || cep.trim() == '')){

            msgError += 'Cep é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!logradouro || logradouro.trim() == '')){

            msgError += 'Logradouro é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!numero || numero.trim() == '')){

            msgError += 'Número é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!telefone || telefone.trim() == '')){

            msgError += 'Telefone é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!meioPagamento || meioPagamento.trim() == '') 
                || meioPagamento.trim() == 'MP'){

            msgError += 'Meio de Pagamento é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!email || email.trim() == '')){

            msgError += 'Email é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }  else if(!this.isEmail(email)){
            msgError += 'Email não está no formato correto!'+ '\n'
            entrouAlgumErro = true
        
        }

        if( (!password || password.trim() == '')){

            msgError += 'Senha é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if(entrouAlgumErro){
            Alert.alert('Dados Inválidos!', msgError)
            return entrouAlgumErro
        }

        return false
        
    }

    isEmail = (email) => {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(mailformat))
            {
            
                return true;
            }
            else
            {
            
                return false;
            }
    }

    salvar = () => {
        
        let encontrouErro = this.validarDados()

        if(!encontrouErro){
            this.props.onCreateUser(this.state)


            Alert.alert(
                //title
                'Cadastro com sucesso',
                //body
                'Cadastro de Cliente efetuado com sucesso!',
                [
                  {text: 'Ok', onPress: () => this.props.navigation.navigate('App2')}
                ],
                { cancelable: false }
                //clicking out side of alert will not cancel
              )

            
        } 
           //Alert.alert('Dados Inválidos!', 'Nome é obrigatório!'+ '\n' + 'Teste')

        //Alert.alert(this.state.name)
    }

    cancelar = () => {
        
        
        this.props.navigation.navigate('Login')
        
    }
    
}

 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center'
    },
    backGround: {
        flex: 3
    },
    formulario: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF'
    },
    input:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    select:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    input2:{
        marginTop: 20,
        width: '80%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    iconBar : {
        flexDirection : 'row',
        marginHorizontal: 20,
        justifyContent: 'space-between',
        marginTop: 10
    }
})

const mapStateToProps = ({user}) => {
    return {
        tipoUser: user.tipoUser        
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateUser: user => dispatch(createUser(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CadastrarUsuario)
//export default CadastrarUsuario