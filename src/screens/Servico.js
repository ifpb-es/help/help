import React, {Component} from 'react'
import {     
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Alert,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { cadastrarServicoTecnico } from '../store/actions/user'

import Icon from 'react-native-vector-icons/FontAwesome'

import moment from 'moment'

class Servico extends Component{


    state ={
        valorServico: '',
        email: '',
        password: ''
    }

    constructor() {
      super();
      this.mascaraMoeda = this.mascaraMoeda.bind(this)
      this.salvar = this.salvar.bind(this)
      //this.enviarAvaliacao = this.enviarAvaliacao(this)
      }



    render(){


        return (            

            <View style={styles.container}>               
                
                <View style={styles.formulario}>
                <ScrollView >

                    <View style={{flexDirection:"row"}}>
                    
                        <Text style={styles.input}>Valor da visita: </Text>

                        <TextInput placeholder='Sugestão: R$40,00' style={styles.input2}
                        autoFocus={true} value={this.state.valorServico} //onKeyPress={valor => this.mascaraMoeda(valor)}
                        //onChangeText={valor =>  this.setState({valor})}/>
                        onChangeText={valorServico => this.mascaraMoeda(valorServico)}/>

                    </View>

                    <TouchableOpacity onPress={this.salvar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cadastrar </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.cancelar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cancelar </Text>
                    </TouchableOpacity>

                </ScrollView>
            </View> 
            </View>    
        )
    }
    
    mascaraMoeda = (valor2) => {
        let valor3 = this.moeda(valor2)

        this.setState({
            valorServico: valor3
          });
    }

    moeda = (v) => {
        v=''+v
        v=v.replace(/\D/g,"") // permite digitar apenas numero
        v=v.replace(/(\d{1})(\d{17})$/,"$1.$2") // coloca ponto antes dos ultimos digitos
        v=v.replace(/(\d{1})(\d{13})$/,"$1.$2") // coloca ponto antes dos ultimos 13 digitos
        v=v.replace(/(\d{1})(\d{10})$/,"$1.$2") // coloca ponto antes dos ultimos 10 digitos
        v=v.replace(/(\d{1})(\d{7})$/,"$1.$2") // coloca ponto antes dos ultimos 7 digitos
        v=v.replace(/(\d{1})(\d{1,2})$/,"$1,$2") // coloca virgula antes dos ultimos 4 digitos
        return v;
    }

    validarDados = () => {
          
        let valorServico = this.state.valorServico
        
        let msgError = ''
        let entrouAlgumErro = false
        
        if( (!valorServico || valorServico.trim() == '')){

            msgError += 'Valor do serviço é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }


        if(entrouAlgumErro){
            Alert.alert('Dados Inválidos!', msgError)
            return entrouAlgumErro
        }

        return false
        
    }

    salvar = () => {
        
        let encontrouErro = this.validarDados()

        if(!encontrouErro){

            let email1 = this.props.email
            let password1 = this.props.password

            this.setState({
                email: email1,
                password: password1
              }); 

              console.log('email',this.state.email)
              console.log('password',this.state.password)

            this.props.onCadastrarServicoTecnico(this.state)

            Alert.alert(
                //title
                'Cadastro com sucesso',
                //body
                'Valor do serviço cadastrado com sucesso!',
                [
                  {text: 'Ok', onPress: () => this.props.navigation.navigate('NavegacaoTecnico')}
                ],
                { cancelable: false }
                //clicking out side of alert will not cancel
              )

        } 
           //Alert.alert('Dados Inválidos!', 'Nome é obrigatório!'+ '\n' + 'Teste')

        //Alert.alert(this.state.name)
    }



    cancelar = () => {
        
        
        this.props.navigation.navigate('NavegacaoTecnico')
        
    }
    
}

 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center'
    },
    backGround: {
        flex: 3
    },
    formulario: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF'
    },
    input:{
        marginTop: 20,
        width: '35%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15,
        paddingTop: 10
    },
    select:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    input2:{
        marginTop: 20,
        width: '60%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    iconBar : {
        flexDirection : 'row',
        marginHorizontal: 20,
        justifyContent: 'space-between',
        marginTop: 10
    }
})

const mapStateToProps = ({user}) => {
    return {
        tipoUser: user.tipoUser,
        email: user.email,
        password: user.password        
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onCadastrarServicoTecnico: user => dispatch(cadastrarServicoTecnico(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Servico)
//export default CadastrarUsuario