import React, {Component} from 'react'
import {     
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Alert,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { obterAvaliacoesTecnico, atualizaEstado } from '../store/actions/user'

import Icon from 'react-native-vector-icons/FontAwesome'
import { Rating, AirbnbRating } from 'react-native-ratings';

import moment from 'moment'

class HistoricoAvaliacaoTecnico extends Component{


    state ={
        valorServico: '',
        email: '',
        password: ''
    }

    constructor() {
      super();
      //this.enviarAvaliacao = this.enviarAvaliacao(this)
      }

     componentDidMount() {
        //this.props.onObterAvaliacoesTecnico(this.state.user)
        console.log('user.email', this.props.email)

        let user ={
            email:this.props.email,
            password:this.props.password
        }

        this.props.onObterAvaliacoesTecnico(user)

        
        console.log("Avaliacoes Tecnico2:", this.props.listaAvaliacoes)
    } 

   /*  componentDidUpdate(prevProps) {
        if (prevProps.listaAvaliacoes !== this.props.listaAvaliacoes) {
            let user ={
                email:this.props.email,
                password:this.props.password
            }
    
            this.props.onObterAvaliacoesTecnico(user)
        }
      } */

    //componentDidUpdate(){

      //  console.log('user.email', this.props.email)

      //  this.props.onObterAvaliacoesTecnico(this.state.user)

   // }

    renderizaRatingComAvaliacoes = () => {

        const items = [];

        let jsonAvaliacoes = this.props.listaAvaliacoes

        for (propertie in jsonAvaliacoes) {
            let value1 = jsonAvaliacoes[propertie]

            console.log('value3', value1)

            for(v1 in value1){
                console.log('value2', value1[v1]) 
                
                items.push(value1[v1])
                
            }
        }
            return (
              <View>
                {items.map((item, index) => 
                   <View>
                    <Text>{ "Nota de " + item.nomeTecnico }</Text>
                    <View style={{flexDirection:"row"}}>
                    
                        <AirbnbRating
                            count={5}
                            reviews={[ "Ruim", "Ok", "Bom", "Ótimo", "Fantástico"]}
                            defaultRating={item.notaAvaliacao}
                            size={20}
                            onFinishRating={this.ratingCompleted}
                            isDisabled={true}
                            /> 
            
                    </View>
                   </View>
                )}
             </View> )

    }

    render(){

        console.log("Avaliacoes Tecnico:", this.props.listaAvaliacoes)

        //console.log("Avaliacoes Tecnico2:", this.state.user.listaAvaliacoes)

        return (            

            <View style={styles.container}>               
                
                <View style={styles.formulario}>
                <ScrollView >

                    {this.renderizaRatingComAvaliacoes()}

                    <TouchableOpacity onPress={this.cancelar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cancelar </Text>
                    </TouchableOpacity>

                </ScrollView>
            </View> 
            </View>    
        )
    }

    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
        this.setState({
          //isStarAberto: false,
          ratingState: rating
        });
      }

    cancelar = () => {
        
        
        this.props.navigation.navigate('App2')
        
    }
    
}

 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center'
    },
    backGround: {
        flex: 3
    },
    formulario: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF'
    },
    input:{
        marginTop: 20,
        width: '35%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15,
        paddingTop: 10
    },
    select:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    input2:{
        marginTop: 20,
        width: '60%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    iconBar : {
        flexDirection : 'row',
        marginHorizontal: 20,
        justifyContent: 'space-between',
        marginTop: 10
    }
})

const mapStateToProps = ({user}) => {
    return {
        tipoUser: user.tipoUser,
        email: user.email,
        password: user.password,
        listaAvaliacoes: user.listaAvaliacoes      
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onObterAvaliacoesTecnico: user => dispatch(obterAvaliacoesTecnico(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoricoAvaliacaoTecnico)
//export default CadastrarUsuario