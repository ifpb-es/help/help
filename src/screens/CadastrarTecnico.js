import React, {Component} from 'react'
import {     
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    DatePickerAndroid,
    Image,
    Alert,
    ImageBackground,
    ScrollView,
    Picker
} from 'react-native'
import { connect } from 'react-redux'
import { createTecnico } from '../store/actions/user'

import Icon from 'react-native-vector-icons/FontAwesome'
import RNPickerSelect from 'react-native-picker-select';

import moment from 'moment'

import { cpf } from 'cpf-cnpj-validator'
import { cnpj } from 'cpf-cnpj-validator'

import backCadastro from '../images/IMG_5984.jpg'


class CadastrarTecnico extends Component{


    state ={
        name: '',
        cpfCnpj: '',
        cep: '',
        logradouro: '',
        numero: '',
        telefone: '',
        qualificacao: '',
        email: '',
        password: ''
    }

    
    render(){

        console.log("Tipo USer:", this.props.tipoUser)

        return (
            <View style={styles.container}>
               
               <ImageBackground source={backCadastro} 
                    style={styles.backGround}>

                    {/* <View style={styles.iconBar}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name='bars' 
                                size={20} color='#000'/>
                        </TouchableOpacity>
                    </View> */}

                </ImageBackground>
                
                <View style={styles.formulario}>
                <ScrollView >
                    <TextInput placeholder='Nome' style={styles.input}
                    autoFocus={true} value={this.state.name}
                    onChangeText={name => this.setState({name})}/>

                    <TextInput placeholder='Cpf/CNPJ*' style={styles.input}
                    autoFocus={false} value={this.state.cpfCnpj}
                    onChangeText={cpfCnpj => this.setState({cpfCnpj})}
                    keyboardType={'numeric'}/>

                    <TextInput placeholder='Cep*' style={styles.input}
                    autoFocus={false} value={this.state.cep}
                    onChangeText={cep => this.setState({cep})}
                    keyboardType={'numeric'}/>

                    <TextInput placeholder='Logradouro*' style={styles.input}
                    autoFocus={false} value={this.state.logradouro}
                    onChangeText={logradouro => this.setState({logradouro})}/>

                    <TextInput placeholder='Número*' style={styles.input}
                    autoFocus={false} value={this.state.numero}
                    onChangeText={numero => this.setState({numero})}/>

                    <TextInput placeholder='Telefone*' style={styles.input}
                    autoFocus={false} value={this.state.telefone}
                    onChangeText={telefone => this.setState({telefone})}/>
                    
                    <Picker selectedValue = {this.state.qualificacao} 
                        onValueChange ={ qualificacao => this.setState({ qualificacao }) }
                        style={styles.select}>
                        <Picker.Item label="Qualificação*" value="Q" />
                        <Picker.Item label="Técnico de Ar-condicionado" value="TA" />                                               
                    </Picker>

                    <TextInput placeholder='Email*' style={styles.input}
                    autoFocus={false} value={this.state.email}
                    onChangeText={email => this.setState({email})}/>

                    <TextInput placeholder='Senha*' style={styles.input}
                    autoFocus={false} value={this.state.password}
                    secureTextEntry={true}
                    onChangeText={password => this.setState({password})}/>

                    <TouchableOpacity onPress={this.salvar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cadastrar </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.cancelar} style={styles.buttom}>
                        <Text style={styles.buttomText}> Cancelar </Text>
                    </TouchableOpacity>

                </ScrollView>
                </View>
            </View>    
        )
    }
    
    validarDados = () => {
          
        let nome = this.state.name
        let cpfCnpj = this.state.cpfCnpj
        let cep = this.state.cep
        let logradouro = this.state.logradouro
        let numero = this.state.numero
        let telefone = this.state.telefone
        let qualificacao = this.state.qualificacao
        let email = this.state.email
        let password = this.state.password

        let msgError = ''
        let entrouAlgumErro = false

        /* if( (!nome || nome.trim() == '')){

            msgError = 'Nome é obrigatório!'+ '\n'
            entrouAlgumErro = true

        } */
        
        if( !cpfCnpj){

            msgError += 'CPF/CNPJ é obrigatório!'+ '\n'
            entrouAlgumErro = true        
        

        }  else if(cpfCnpj.length < 11 || cpfCnpj.length > 11){
            msgError += 'CPF/CNPJ deve conter 11 dígitos!'+ '\n'
            entrouAlgumErro = true
        
        } else if(!cpf.isValid(cpfCnpj)){
            msgError += 'CPF/CNPJ não é válido!'+ '\n'
            entrouAlgumErro = true
        }
        
        if( (!cep || cep.trim() == '')){

            msgError += 'Cep é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!logradouro || logradouro.trim() == '')){

            msgError += 'Logradouro é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!numero || numero.trim() == '')){

            msgError += 'Número é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!telefone || telefone.trim() == '')){

            msgError += 'Telefone é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!qualificacao || qualificacao.trim() == '' 
                || qualificacao.trim() == 'Q')){

            msgError += 'Qualificação é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if( (!email || email.trim() == '')){

            msgError += 'Email é obrigatório!'+ '\n'
            entrouAlgumErro = true

        } else if(!this.isEmail(email)){
            msgError += 'Email não está no formato correto!'+ '\n'
            entrouAlgumErro = true
        
        }

        if( (!password || password.trim() == '')){

            msgError += 'Senha é obrigatório!'+ '\n'
            entrouAlgumErro = true

        }

        if(entrouAlgumErro){
            Alert.alert('Dados Inválidos!', msgError)
            return entrouAlgumErro
        }

        return false
        
    }

    isEmail = (email) => {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(email.match(mailformat))
            {
            
                return true;
            }
            else
            {
            
                return false;
            }
    }

    salvar = () => {
        
        let encontrouErro = this.validarDados()

        if(!encontrouErro){
            this.props.onCreateTecnico(this.state)
            

            Alert.alert(
                //title
                'Cadastro com sucesso',
                //body
                'Cadastro de Técnico efetuado com sucesso!',
                [
                  {text: 'Ok', onPress: () => this.props.navigation.navigate('NavegacaoTecnico')}
                ],
                { cancelable: false }
                //clicking out side of alert will not cancel
              )
        }
        //Alert.alert('Dados Inválidos!', 'Nome é obrigatório!'+ '\n' + 'Teste')

        //Alert.alert(this.state.name)
    }

    cancelar = () => {
        
        
        this.props.navigation.navigate('Login')
        
    }
    
}

 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'center'
    },
    backGround: {
        flex: 3
    },
    formulario: {
        flex: 7,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'row',
        paddingLeft:20
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF'
    },
    input:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    select:{
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    input2:{
        marginTop: 20,
        width: '80%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333',
        paddingLeft: 15
    },
    iconBar : {
        flexDirection : 'row',
        marginHorizontal: 20,
        justifyContent: 'space-between',
        marginTop: 10
    }
})

const mapStateToProps = ({user}) => {
    return {
        tipoUser: user.tipoUser        
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateTecnico: user => dispatch(createTecnico(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CadastrarTecnico)
//export default CadastrarUsuario