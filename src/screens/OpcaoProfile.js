import React, { Component } from 'react'
import { 
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TextInput
} from 'react-native'
import { connect } from 'react-redux'

import { login } from '../store/actions/user'
import { entrarTipoUsuario } from '../store/actions/user'

class OpcaoProfile extends Component {
    state = {
        tipoUser: ''
    }

    componentDidUpdate = prevProps => {
        if(prevProps.isLoading && !this.props.isLoading){
            //this.props.navigation.navigate('Profile')
            this.props.navigation.navigate('App2')
        }
    }

    entrarComoCliente = () => {
        this.state.tipoUser = 'C'

        console.log("Tipo User ", this.state.tipoUser)
        this.props.onEntrarComoCliente({ ...this.state })

        this.props.navigation.navigate('Login')
    }

    entrarComoTecnico = () => {
        this.state.tipoUser = 'T'

        this.props.onEntrarComoTecnico({ ...this.state })

        this.props.navigation.navigate('Login')
    }

    login = () => {
        this.props.onLogin({ ...this.state })
        //this.props.navigation.navigate('Profile')
        this.props.navigation.navigate('App2')
    }

    cadastrarUsuario = () => {
        this.props.navigation.navigate('CadastrarUsuario')
    }

    render () {
        return (
            <View style={styles.container}>
                
                <TouchableOpacity onPress={this.entrarComoCliente} style={styles.buttom}>
                    <Text style={styles.buttomText}>Entrar como Cliente</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.entrarComoTecnico} style={styles.buttom}>
                    <Text style={styles.buttomText}>Entrar como Técnico</Text>
                </TouchableOpacity> 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttom: {
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f4b042',
        borderRadius:10        
    },
    buttomText: {
        fontSize: 20,
        color: '#FFF',
    },
    input: {
        marginTop: 20,
        width: '90%',
        backgroundColor: '#EEE',
        height: 40,
        borderWidth: 1,
        borderColor: '#333', 
    }
})

const mapStateToProps = ({user}) => {
    return {
        isLoading: user.isLoading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onEntrarComoCliente: user => dispatch(entrarTipoUsuario(user)),
        onEntrarComoTecnico: user => dispatch(entrarTipoUsuario(user))
    }
}

//export default Login
export default connect(mapStateToProps, mapDispatchToProps)(OpcaoProfile)